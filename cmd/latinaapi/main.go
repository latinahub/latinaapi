package main

import (
	"os"
	"time"

	"github.com/go-co-op/gocron"
	_ "github.com/mattn/go-sqlite3"

	"github.com/LatinaHub/LatinaApi/api/router"
	"github.com/LatinaHub/LatinaApi/cmd/dl"
	"github.com/LatinaHub/LatinaApi/internal/db"
)

func cronJob() {
	schedule := gocron.NewScheduler(time.UTC)
	schedule.Every(30).Minutes().Do(func() {
		dl.DownloadResource()
	})

	schedule.StartAsync()
}

func checkDir() {
	_, err := os.Stat("resources")
	if err != nil {
		panic("Could not find resources folder, exiting...")
	}
}

func main() {
	// Check directory
	checkDir()

	// Set cron job to Download database
	cronJob()

	// Initialize databse
	db.Database.Init()

	// Start the router
	router.Start()
}
